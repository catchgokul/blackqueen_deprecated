from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from google.appengine.ext import db

from app.models import *

import os, sys, datetime, copy, logging, settings


# Change everything in this class! Makes things pretty fast and easy
# so the basic info about the site is ubiquitous. 
class globvars():
  pages = [
      {'name':'Home', 'url':'../../'}, # add pages here
      {'name':'About', 'url':'../../about'},
    ]
  proj_name = "Ultimate Black Queen" # change this!
  founders = [
    {'name':'Gokul Menon', #obviously, not you. 
       'email':'gokulm@rutgers.edu',
       'url':'http://paul.rutgers.edu/~gokulm',
       'blurb':'I\'m Gokul, just playing around, trying to learn this stuff.',
       'picture':'https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-ash4/260398_10150650939680612_521855611_19372976_3082320_n.jpg'}, #to self-host, put images in /front-end/media/images 
    ]
  originalAuthor =  {'name':'Alex Rattray',
       'email':'rattray@wharton.upenn.edu',
       'url':'http://hackpack.decisioncandy.com/about/',
       'blurb':'I\'m Alex. I like webdev and most things Seattle.',
       'picture':'http://profile.ak.fbcdn.net/hprofile-ak-ash2/273392_515004220_1419119046_n.jpg'}
  proj_description = "A quick and dirty package of Django on Google App Engine running Python 2.7 with HTML5 Boilerplate and Twitter Bootstrap."
  context = {
      'pages': pages,
      'proj_name': proj_name,
      'founders': founders,
      'originalAuthor':originalAuthor,
      'proj_description': proj_description,
      }
  
def about(request):
    gv = globvars
    context = {
      'thispage':'About'
        }
    context = dict(context, **gv.context)
    return render_to_response('about.html',context)
  
def index(request):
  gv = globvars
  context = {
    'thispage':'Home'
      }
  context = dict(context, **gv.context) #combines the 'local' context with the 'global' context
  return render_to_response('index.html', context)
